controlling
ifTrue: alternativeBlock1 else: aCondition2 ifTrue: alternativeBlock2 else: aCondition3 ifTrue: alternativeBlock3
	"If the receiver is false (i.e., the condition is false), then the value is the 
	false alternative, which is nil. Otherwise answer the result of evaluating 
	the argument, alternativeBlock. Create an error notification if the 
	receiver is nonBoolean. Execution does not actually reach here because 
	the expression is compiled in-line."

	^ aCondition2 ifTrue: alternativeBlock2 else: aCondition3 ifTrue: alternativeBlock3