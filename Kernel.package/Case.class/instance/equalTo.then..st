accessing
equalTo: anObject then: execBlock
	"The oneArgTestBlock must return a Boolean value when passed the criterion of the receiver."

	self case: [ :aCriterion | aCriterion = anObject ] then: execBlock.