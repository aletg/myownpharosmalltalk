"
False defines the behavior of its single instance, false -- logical negation. Notice how the truth-value checks become direct message sends, without the need for explicit testing.

Be aware however that most of these methods are not sent as real messages in normal use. Most are inline coded by the compiler as test and jump bytecodes - avoiding the overhead of the full message sends. So simply redefining these methods here will have no effect.
"
Class {
	#name : #False,
	#superclass : #Boolean,
	#category : #'Kernel-Objects'
}

{ #category : #controlling }
False >> ifTrue: alternativeBlock1 else: aCondition2 ifTrue: alternativeBlock2 [
	"If the receiver is false (i.e., the condition is false), then the value is the 
	false alternative, which is nil. Otherwise answer the result of evaluating 
	the argument, alternativeBlock. Create an error notification if the 
	receiver is nonBoolean. Execution does not actually reach here because 
	the expression is compiled in-line."

	^ aCondition2 ifTrue: alternativeBlock2.
]

{ #category : #controlling }
False >> ifTrue: alternativeBlock1 else: aCondition2 ifTrue: alternativeBlock2 else: alternativeBlock3 [
	"If the receiver is false (i.e., the condition is false), then the value is the 
	false alternative, which is nil. Otherwise answer the result of evaluating 
	the argument, alternativeBlock. Create an error notification if the 
	receiver is nonBoolean. Execution does not actually reach here because 
	the expression is compiled in-line."

	^ aCondition2 ifTrue: alternativeBlock2 ifFalse: alternativeBlock3.
]

{ #category : #controlling }
False >> ifTrue: alternativeBlock1 else: aCondition2 ifTrue: alternativeBlock2 else: aCondition3 ifTrue: alternativeBlock3 [
	"If the receiver is false (i.e., the condition is false), then the value is the 
	false alternative, which is nil. Otherwise answer the result of evaluating 
	the argument, alternativeBlock. Create an error notification if the 
	receiver is nonBoolean. Execution does not actually reach here because 
	the expression is compiled in-line."

	^ aCondition2 ifTrue: alternativeBlock2 else: aCondition3 ifTrue: alternativeBlock3
]

{ #category : #controlling }
False >> ifTrue: alternativeBlock1 else: aCondition2 ifTrue: alternativeBlock2 else: aCondition3 ifTrue: alternativeBlock3 else: alternativeBlock4 [
	"If the receiver is false (i.e., the condition is false), then the value is the 
	false alternative, which is nil. Otherwise answer the result of evaluating 
	the argument, alternativeBlock. Create an error notification if the 
	receiver is nonBoolean. Execution does not actually reach here because 
	the expression is compiled in-line."

	^ aCondition2 ifTrue: alternativeBlock2 else: aCondition3 ifTrue: alternativeBlock3 else: alternativeBlock4
]
