accessing
default: execBlock
	"The oneArgTestBlock must return a Boolean value when passed the criterion of the receiver."

	satisfied ifFalse: [ response := execBlock value. ].
	
	^ response.