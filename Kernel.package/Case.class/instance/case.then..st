accessing
case: oneArgTestBlock then: execBlock
	"The oneArgTestBlock must return a Boolean value when passed the criterion of the receiver."

	satisfied ifFalse: [
		(oneArgTestBlock value: criterion) ifTrue: [ 
			response := execBlock value.
			satisfied := true.
		].
	].