Class {
	#name : #Case,
	#superclass : #ProtoObject,
	#instVars : [
		'criterion',
		'satisfied',
		'response'
	],
	#category : #'Kernel-Objects'
}

{ #category : #'instance creation' }
Case class >> for: anObject [

	^ self new criterion: anObject 
]

{ #category : #accessing }
Case >> case: oneArgTestBlock then: execBlock [
	"The oneArgTestBlock must return a Boolean value when passed the criterion of the receiver."

	satisfied ifFalse: [
		(oneArgTestBlock value: criterion) ifTrue: [ 
			response := execBlock value.
			satisfied := true.
		].
	].
]

{ #category : #accessing }
Case >> criterion: aCriterion [
	
	criterion := aCriterion.
	satisfied := false.
]

{ #category : #accessing }
Case >> default: execBlock [
	"The oneArgTestBlock must return a Boolean value when passed the criterion of the receiver."

	satisfied ifFalse: [ response := execBlock value. ].
	
	^ response.
]

{ #category : #accessing }
Case >> equalTo: anObject then: execBlock [
	"The oneArgTestBlock must return a Boolean value when passed the criterion of the receiver."

	self case: [ :aCriterion | aCriterion = anObject ] then: execBlock.
]
